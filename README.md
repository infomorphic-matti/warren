# warren
3D printing intro guides all say the same thing - "you should 3D print in a separate room with good ventilation". Most printing communities assume you have lots of physical space, or design things upon the assumption of access to (often expensive) powertools that require lots of space to store and lack of proximity to other people to use without disturbing them. People suggest printing chambers with price points >£100 to print materials which emit fumes that are not exactly healthy. Storage of large amounts of filament (when a dry box is not involved - though that also is expensive) typically involves mounting shelves on walls, which is often not possible. 

This project wants to change all that. Many people live in smaller apartments and shared accommodation - including the original author of this project. This may be by choice, may be by financial need, or may be by both. Nowadays, budget printers (sub £200) exist which are powerful enough to do meaningful printing, and with cheap modifications that can enable the use of more difficult printing materials.

The `warren` project is then a repository of information documenting and providing files for the creation of open-hardware systems designed to enable 3D printing in extremely confined spaces and living conditions - as well as other tools/infrastructure designed for smaller spaces and their common associated difficulties, both for general living and more "specialised" things like mini-labs, manufacturing, or research spaces. It's meant in the same spirit as the RepRap project - open source, open hardware, open collaboration, DIY - and the idea of using a 3D printer to bootstrap your own setup and environment, and give you more power over your own living space and the ability to *make* space to create and build other things. 

The name, then, refers to the idea of a rodent warren - in a positive or neutral sense, as in, being around others in dense spaces - as well as the kind of high-density, almost fractal, structure required to make the goals of the project viable. It hopefully exemplifies a kind of extreme adaptivity, agency, and creation that can still thrive in environments that are hostile to traditional methods of doing those things while still being highly generalised and capable - and by proxy, far more accessible for people in situations other than "decent-sized middle-class house/apartment with access to decent financial resources". 

Designs are also intended to be disassemble-able into parts which are *compact* where possible (e.g. pipes should be able to be split into components that can be densely stacked), as people in smaller living situations on a low budget are also likely to be in more unstable housing situations and being able to fit their setup into as few boxes as possible will save money and effort if they need to move.

## Getting Started
If you've just bought a 3D printer (or are considering one) - and are in a situation like that described above (or just interested in the concept), the first place to look is in [The Top-Level Bootstrap File](/bootstrap.md). Also consider reading the notes on [Using the OpenSCAD (or other) models](/using-the-models.md).

This explains things like what you need to consider when obtaining a filament, why we need to "bootstrap" at all instead of just using the 3D printer & filament where-ever, and tips and tricks for getting started with printers (right now, this is only applicable to the printer I've obtained - the Creality Ender 3V3 SE, but the hope is that people will start adding more printers, and adding more RepRappy/DIY/FOSS printers, or anything else really). 

If you haven't bought or built a printer yet, consider reading the [section on Tyrant Hardware first](#note-on-tyrant-hardware-printers), as it is relevant to what this project is able to help with, your rights and autonomy as someone owning a printer, and the health of the 3d printer community as a whole (along with some of the history). 

## This Repo - Organisation & Contributions
This repository is currently the home of the `warren` project. In many ways, this is also documenting the initial author's own 3D printing "bootstrapping", so at the start, expect commits to be structured like someone working through and developing them (because that's exactly what is happening).

### Folder Structures
* `bootstrap/` - This contains files and folders for bootstrapping from just a 3D printer and a bit of PLA filament (as well as other rolls of filament types that can't be used until you reach a certain degree of printer capability/environment) to a proper 3D printer setup. It is split into stages. Many resources, files, and information in this section are also useful in any other sections and might be linked from there. 
* `main/` - This contains infrastructure for modular, high-density organisation in 3 dimensions. This is designed to enable highly compact containment of your 3D printer and related things, as well as slot that in to a larger, fractal system. This folder internally shall have other folders and similar things for organisation:
    * Along with this, it contains folders for any other useful infrastructure, devices, or utilities useful for living in smaller spaces. 
    * This folder ideally should be well organised.
* `extern/` - Contains 3rd party printable files and any docs. They must be under licenses compatible with this project. 
* `sourcing/` - This folder contains information on where and how to source components that you can't print (or may be more effective if they are manufactured by traditional manufacturing methods). 
    * Naturally, this project attempts to avoid non-printable things as much as possible, and if you are listing a part that has a printable alternative, try to include that printable part or actually create one. 
    * This folder is also not an advertisement section, and the expectation is that both advantages and drawbacks are honestly listed along with sourcing for parts - including things like delivery options (some countries or locations may have trouble with deliveries or in-person pickup, or there may be legal issues). 
    * You should try and avoid ephemeral listings for parts that are likely to vanish within a couple weeks. 
    * If a part is relatively "standard", then it's strongly encouraged for people to find multiple distinct sellers of the same part matching the same specs, and contributors must make an effort to note the fact it *is* a standard part. 
    * Clones of "official" parts that are associated with a corporation are encouraged, triply-so if they are cheaper than originals by a significant amount, but make sure to mention any drawbacks that are common with clones. Some 3d-printer companies do have relatively affordable replacements.
    * Try to keep things organised - in particular, generic parts that follow a specification should be inside a folder (inside any other relevant folder categories) that refers to that specification. If a specification is modular (e.g. it's "parameterised" by lengths, or diameter, or shape, or something) then there should be a top level folder for the entire generic standard and subfolders for each instantiation - if there end up being too many folders, then splitting out into multiple levels - one for each generic spec variable - is encouraged. If even that is not enough, or there is one variable with a vast number of variants, then using subfolders over ranges of a variable's possible values should be done.  
* `mods/` - This folder is for mods for specific 3d printers. Guides on installing, repairing, and maintaining them are encouraged, and use of generic clones is acceptable as long as both the risks and advantages are properly accounted for or documented.

### Documentation (Markdown). 
Documentation in this project is generally written as markdown files, with the same name (but different extension) to the files (or subfolders) they're referring to. For folder structure, this is similar to how you'd organise a modern Rust project, but with `.md` instead of `.rs`. 

For code files, if there is a small amount of documentation, it's fine for it to be embedded in the code files, but beyond a certain point you should move it out and into a markdown file. To link to other parts of the project, use markdown links (preferred for compatibility with other markdown parsers) - either absolute (from the root of the project) or relative to the page location.

### OpenSCAD Code
If you're using some OpenSCAD code files from other OpenSCAD code files, then you should use *relative links* to the code file itself (or if using a well-organised bundle in the repo) to a relevant folder containing the relevant OpenSCAD files. That is, create a link using the command `ln -rs [location of file or library-folder inside repo] [target-name]`. Then you can use the [openscad `use` command](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Include_Statement) to import things from those files or folders. Using the `include` statement is strongly discouraged as it is less clean.

Unfortunately OpenSCAD lacks a lot of the power of a normal programming language, which means proper module handling is nastier and there is little in the way of strong types to make editing easier. 

When writing components and parts, you should separate them into sensible folders. So if you have a composite project, then that should have a dedicated folder, and each relevant part should have a dedicated OpenSCAD file. Common functionality can be put in another internal file they include. This is a recursive process, so if you then have a part that is composed of others, it should have it's own folder with it's subparts.

This allows for easily making modular parts, and reusing sub-components of a project as a component in another project, by moving it out into it's own mini library folder then linking that everywhere that should use it. In the same vein, if a part is parameterisable, and subparts are all parameterisable, etc., then each part should take parameters rather than doing something janky like `include`. Then, the combining part (and it's associated modules) can pass in the relevant variables to the sub-parts it is composed of. 

If a part is not just programmatically composed of other parts, but *physically* composed of other parts (i.e. you physically combine subparts after production), then this still applies, but the composed part should have an OpenSCAD file that contains modules calling the actual modules for each subpart using variables specifiable at the top of the file. This way, any user can modify the variables in the common file just once, and then they can call the various wrapper modules with no parameters to create any of the subparts with the relevant dimensions, or they can use the `include` command in their own dummy OpenSCAD file and set the variables after that to overwrite them, if they don't want to worry about getting the git repo in a weird state. This also lets you do nice things like provide partially pre-configured versions of very modular parts.

When rendering or defining a part with any curved components, you should *always* define a high `$fn` value (at least 360) for smooth rendering.

### Policy on Printer/Hardware-Specific Information
This repository is general purpose, ideally. However, it is also encouraged for people to contribute printer- and hardware-specific information, sourcing, files, etc. in the documentation, as long as it's clearly annotated as such. The creator of the repository will be doing the same, after all. 

This can be put directly in documentation files, inline, as long it's clear. Consider using subheaders for descriptive annotation, or a table for numerical/data association, and if it starts to get really unwieldy in a given section, consider splitting it out into separate files per-printer/hardware in a subdirectory for the subtopic and then creating a table in the main document linking to the relevant files. All subfolders for a given documentation file should themselves be inside a directory of the name of the main documentation file e.g.:
```txt
- my-general-bootstrap-method.md
- my-general-bootstrap-method/
    - thing-with-lots-of-printer-or-hardware-specific-docs/
        - bambu/
            - a1.md
        - creality/
            - ender-3.md
            - ender-3v3-se.md
        - foldarap.md
        - prusa/
            - i3-mk2.md
            - mk4.md
            - mk4/
                - mmu3.md
        - ratrig/
            - v-core-4.md
            - v-core-2.md
        - voron/
            - V2.md
    - thing-with-more-hardware-specific-info/
        - special-motor-1234.md
        - special-wiring-type-38392.md
        - extra-magical-electronics-here.md
```

Essentially, this should be organised logically like any other resource used in a documentation page (images, files, etc.).

#### Note on "Tyrant Hardware" Printers
This project encourages the people using it to avoid "tyrant hardware" that locks down a printer or it's firmware from real modification and replacement. 

For now, the primary up- and coming culprit of this is Bambu (who are often described as the "Apple" of 3D printing, which the author would agree with but in a very negative sense), but the 3D printing world has gone through several iterations of companies trying to do this either via actual locking-down or via patents (or both). The RepRap project itself only finally got a solid kickstart when the patent for Fused Filament Fabrication (the plastic extrusion method of production) expired in 2009 after being held in stranglehold by a company called Stratasys for well over a decade. 

Even if they might seem cheap, they fundamentally choke out your ability to control your own hardware, not merely shirking the open source work that has gone into making 3D printing accessible (most companies do this to some degree, see Creality and it's continual GPL compliance issues..., and even Prusa has had issues despite being the "face" of commercial FOSS printers) - but instead actively spitting on it by trying to stop other people from finding open-source ways to change and use their hardware (via DRM, locking down, patents, etc.). Even if they seem to cooperate when someone finally cracks it, they usually make sure they still retain decent control over the architecture and the core parts. 

No matter how "friendly" or "down to earth" their CEOs might seem, the actual practice is user- and society- hostile and while they can act to push the rest of Open 3D printing to advance (which is beneficial to us), the initial author strongly encourages you to avoid these printers. Contributing things for the project that is specifically related to companies doing things like this may put the project itself at risk due to patents or other related "intellectual property" constructions. We'll still accept them, of course, as this project is meant to aid people to work with what they've got and give themselves much more powerful manufacturing capabilities - but it carries risk and ethical issues. 

These printers or pieces of hardware also have a significant downside for the specific goals of this project - it makes sure you can't upgrade or modify your printer effectively and affordably, or use more advanced software to get large increases in performance even on weaker printers. It puts a hard cap on what you can truly do with the hardware you have and can afford, the antithesis of the entire purpose of this project and something it stands strongly against. 

We'd recommend putting that effort you would otherwise use to support those printers towards finding ways to create improvement in the Free & Open Source printer space, via several methods (not an exhaustive list at all):
* Contribute to open standards that can enable smooth ("zero- or minimal-config"), user-friendly integration between 3D printers and their administration hardware (or within a 3D printer itself e.g. via hardware standards). These standards should be modular to allow for the continuing rapid advancement of 3D printing tech, and should encompass things like automatic bed levelling, resonance detection + input-shaping, etc. G-Code often can get a printer to do these things, and creating a database in a standard, open format for this might be a helpful direction.  
* Contribute to FOSS software for 3D printing/additive-manufacturing and CAD itself, to make it more user friendly and more automatic e.g. by adding information about different hardware, or auto-detection, or improving the User Interface, or, very importantly, improving the documentation and translations of the software. Essentially, make the claim "proprietary and highly integrated but also highly corporate-controlled = more user friendly" a false one. 
* Develop new designs for alternate implementations of technology (like automatic material/filament switching) used by these sorts of hyper-proprietary printers, ensuring that they don't conflict with the specific patents. Then, open source them, and consider placing them on the RepRap wiki (as RepRap keeps good records of when ideas are contributed, and should help avoid any attempts at patents, since it's also a very nice public location and should count as "prior art" that's easy to find). If you use a license like the Open Hardware License v2 (-S or -W or -P) it should also add additional defence against patent attempts (GPL does this too, but it is less suited for hardware). Then, the open source community benefits as a whole from advancement. 
* Help the RepRap project and similar efforts to make additive manufacturing accessible and more self-replicating, as this can allow people to avoid supply issues and proprietary lock-in via traditional manufacturing methods used to create non-standard/proprietary parts. 

We still accept contributions relating to these printers - as working with what you have is an important aspect of this project - but we must be careful with it, and honestly we'd encourage you to use it to build some kind of RepRap with a large number of printed parts (for affordability) instead, or build a more open-source printer, or just buy something else if you haven't already committed to such tyrant hardware. Take advantage of online guides and documentation if it's a little more DIY and you're worried about that (though my experience is that modern printers that aren't designed as kits are pretty simple and reliable to assemble - even as someone who'd never done it before it took me less than 40 minutes and that was with me messing around and losing track of things a lot, because of distraction and a very non-ideal work area with lots of places to lose things).

### Licensing
The Free/Open-Source Hardware nature of this repository is also related to it's licensing:
* Any hardware files/hardware - like OpenSCAD files - are licensed under the [Open Hardware License v2-or-later](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2), in the Strongly Reciprocal variant. There is also a Notice (in `NOTICE-CERN-OHL-S-2.0+`) regarding ensuring the visibility of a Source Location.  
    * Full SPDX of this setup: `CERN-OHL-S-2.0+`
* The textual documentation is licensed under the following 3 licenses:
    * The same CERN OHL v2+ as the hardware files/hardware, other than the source location prominence notice as it's only relevant to hardware distribution.  
    * CC-BY-SA 4.0 - for compatibility with RepRap wiki
    * GNU Free Documentation License 1.2 or later - for compatibility with RepRap wiki should they wish to include things from this project.
    * Full SPDX of this setup: `CERN-OHL-S-2.0+ OR CC-BY-SA-4.0 OR GFDL-1.2-or-later`
* Any software package that happens to be generated during this process, is licensed under GNU AGPLv3-or-later. Because CERN OHL v2 allows for Available Components, and the software would be published openly in the repository, such software should be considered an Available Component - though CERN OHL doesn't actually consider software uploaded onto hardware to be a component at all unless it's gateware, or it's specific-purpose enough to act as a hardware design schematic in-itself (note that this means the "software" category should not be used to directly encode hardware or sources for hardware, and should only include things like slicers or other processing tools, such that the hardware is not considered as a derivative of the AGPLv3+ software):
    * Full SPDX of this setup: `AGPL-3.0-or-later`

### Contributing
If you contribute to the project, your contributions must be under a licensing scheme compatible to be included in the relevant licenses specified above. Without explicit notice otherwise, your contributions are assumed to be under the same licenses as above (note - we explicitly LACK any sort of Contributor Licensing Agreement for the project, we can't turn around and relicense any contributions in some proprietary way). 
