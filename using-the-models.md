# Using The Models
In this project, there are a fair number of 3D models - currently, they're mostly written in [openscad]. Because of the need for adaptability, most of these are parametric-ish and you'll need to do some (very basic) imports and module calls to use them (mostly just setting variables to change lengths really).

When you build your modules we recommend using the `.amf` output format - it's more modern, can handle curves, etc. The way to do this is simply call `openscad <your openscad file here>.scad -o <your output file here>.amf`. In the `OpenSCAD` gui, you can do this by simply going `File > Export > Export as AMF`.

Then, you can import the exported model in your slicer. If any specific slicer stuff is needed, that should be documented inside the OpenSCAD modules you used or inside the associated external Markdown documentation. For bulky models, if there isn't a lot of detail in large areas, it would often be worth increasing the layer height for those regions of the object. 

The git repository is set up to ignore files that are not bare OpenSCAD models - in particular, AMF, 3MF. This way you can mess around with 3D models without worrying about them getting accidentally committed. 

If you want to contribute models in other formats, you're welcome to do so, as long as those formats are open formats (i.e. the "canonical" implementation is fully FOSS). This is so that everyone using the repository has the ability to use the models.

[openscad]: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual "OpenSCAD"
