# Bootstrapping
For this project, bootstrapping refers to the fundamental idea of obtaining a 3D printer, some basic filament (and more advanced filament), then using that to create structures for safer and more effective 3D printing.

This is because inhaling 3D printing fumes is not-great to extremely-bad for you. The goal of bootstrapping is, then, to create a 3D printer chamber. This chamber should also be strong enough and provide access points such that it can be moved around and manipulated like any other stored object. 

The bootstrapping process also establishes some of the "standards" (for dimensions) that will be used for later aspects of the `warren` system (e.g. ventilation and wiring standards, component sizes, connectors, etc.).

Skip forward:
* [to filament properties and considerations around humidity](#considerations-filament)
* [to information on slicers](#slicers)
* [to information on printer beds & tips for using them](#printer-beds)
* [to an overview of the bootstrap stages](/bootstrap/stage.md#bootstrap-stages-an-overview)

## Considerations - Printer
How you obtain the 3D printer is up to you - you can go for something very cheap but premade or mostly-premade, or go full-on RepStrap and bootstrap your way completely from scratch (though this option may result in slower printing for comparable price, even if we're going to be using software to increase the power and capability - it, however, also might be better). You need to consider availability in your local region regardless. We also recommend the [reading the notes on tyrant hardware](/README.md#note-on-tyrant-hardware-printers) before any purchase. 

### Auto-Leveling
One of the major benefits of recent printer designs is that they include automatic leveling z-offset measurement and mesh bed levelling (which lets printers adjust for minor variations in the bed height). It's also often possible to (fairly cheaply) upgrade a printer to have this capability by modifying the hot-end with a sensor and using modified firmware. 

This makes it much easier (in fact almost trivial in my experience) to get good adhesion of any prints on the print bed as long as you have the right temperature, though you might still have to tune a first layer sometimes. 

You can do z-levelling manually as well (that's not in this file, but there are lots of resources out there on it and if someone wants to contribute tips on that they can), and you can do some degree of bed levelling manually (but usually only 4 corners). 

Full mesh levelling typically has a higher degree of accuracy than this manual levelling, and is automated - though if you have very large deviations you will probably want to adjust the print bed by loosening/tightening screws or reducing the height of the inserts at the corners to make the bed flatter.

If someone wants to embed information on these more manual processes, they should feel free to do so, though there are plenty of other external resources on it.

### Hardware Print Speed
I'd recommend using a printer that has hardware capable of decent printing speeds (on the order of 250mm/s, which the Ender 3V3 SE is at least theoretically capable of), even if the software on the printer is not currently capable of it. We'll probably end up using Klipper firmware fairly quickly, as it has powerful input-shaping capabilities and pressure-advance capabilities that can take full advantage of the hardware as long as you can follow tuning steps.

### Extruder Type
You also will almost certainly want a printer with a Direct Drive Extruder rather than a Bowden Extruder. A direct drive extruder places the gears that control how the filament is fed directly next to the nozzle, whereas a Bowden extruder places the gears far away, and then uses a tube lined with PTFE coating to move the filament. 

This fundamentally restricts the materials that can be printed effectively (as many types will not slide through the tube well, or will strip the coating rapidly), and also causes temperature challenges in the hot-end because PTFE degrades into highly toxic fumes above 260°C (and emits fumes very slowly from ~240°C). 

This is also an issue with PTFE-lined nozzles, and it will probably be worth it for many folks to replace theirs with a fully metal nozzle when they want to push past the temperature boundaries.

Most materials that are not PLA or TPU require temperatures high enough that the fumes are a risk, especially beyond 245°C, and one of the main goals of this project is being able to use different materials than simple PLA, for reasons described in the [Filament Considerations](#considerations-filament) section. We will be printing PETG using such a bronze+PTFE extruder (as I'm using an Ender 3V3 SE), but this is very much pushing the limits, and for more high-temp or abrasive materials like ABS I'll also need to upgrade the extruder).

Bowden extruders do have some advantages - related to tangles and print-head mass, and simplicity, but most of the corresponding disadvantages of direct-drive extruders can be mitigated via software, and a major advantage of direct-drive extruders is their ability to print more flexible filament and more types of material in general, which we'll be taking significant advantage of over time to build infrastructure around ventilation.

### Physical Print Size
This project is very much focused on building infrastructure in highly confined living areas. As such, the physical dimensions of a printer are a somewhat important consideration. 

There are printers with extremely small footprints (on the order of 10cmx10cm) like some of the Monoprice ones, but these often have issues with moddability for non-PLA materials, and the limits of build size themselves start to cause issues when you want to do larger scale construction. 

The other consideration is the type of printer. In particular, a lot of more affordable printers use a "bedslinger" design, which is where the printing area moves horizontally in one of the axes, rather than vertically (which would typically be CoreXY designs). This means they take up more horizontal space in practice, and you need to account for that.

All in all though, physical printer size is not a huge issue unless you go for large printers - and one of the goals in this project is the creation of highly vertical storage which means it should still have relatively low effect on your actual usable space. The main issue is all the infrastructure *surrounding* the printers, which must be made compact, as well as the filament storage and a system to mount filament for printer usage while avoiding breaking any seals for the more damp-sensitive filament types. 

### Filament Diameter 
The printer you obtain or have, should be made to work with 1.75mm-diameter filament if at all possible. 3mm-diameter filament is a dying standard and it's best if you either obtain a printer that uses 1.75mm, or modify your printer to use 1.75mm. Almost all modern printers should work with 1.75mm filament, so when obtaining a new printer you'll probably be fine. 

## Considerations - Filament
There are a large number of different types of 3D printing filament available. We must consider them based on material properties and what they need during printing. 

When living in a smaller space, on very small amounts of money, there are common issues that should affect your material choice along with the standard aspects of material properties:
* Filament diameter - Most modern filament is of diameter 1.75mm. Your printer probably uses 1.75mm filament, and most infrastructure is designed from 1.75mm filament. 3mm-diameter filament used to be common, but now it's very rare - I'd recommend avoiding it, as it's a dying standard, as far as I can tell. 
* A lot of smaller spaces, especially with scummier-than-usual landlords or just poor construction - or location in unfortunate climates - may suffer from chronically high humidity (and mould/mildew). My living area hovers around 75% humidity, and often gets as high as 80% or above (though sometimes gets lower if I'm lucky). This degrades filament within days (or for some important materials, hours), and some filament types like PLA are still vulnerable after construction. PLA in particular becomes brittle over time and can fail suddenly and catastrophically - which, if it's being used to support your 3D printer and important stuff (that presumably you acquired on a very small budget or otherwise can't afford to lose) - is pretty bad.  
* Things requiring a heated chamber are an extra challenge if you can't just buy one. If you can, a lot of this bootstrap process is unnecessary as the initial bootstrapping is primarily for obtaining ventilation (and a heated chamber implies that). Of course, with sufficient insulation it's still doable to print materials like this by capturing the heat from the bed and nozzle, but it needs to be worked up to. 
* Ventilation itself - you need at least one material (probably PLA) that you can print without the fumes being imminently dangerous in small quantities (though inhaling them is a bad idea for any filament). The major goal of the earliest bootstrap stages are constructing progressively superior containment chambers and piping, with the initial one using an extremely minimal amount of filament. Something like ABS - which has toxic fumes - is not safe to print without a good ventilation (and ideally, filtering) system. 
    Note - we can now bootstrap using zero filament to an initial chamber, using only common materials like cardboard, tape, (preferrably) gluestick, paper, and bog-standard stationary like pencils/pens, rulers, and scissors.   

The primary materials we suggest to work with for bootstrapping are as follows:
* PLA. If you buy a printer, they often come with a small amount of sample PLA filament, which should be sufficient for our needs in constructing stage-0 ventilation bootstrap (note - we now have construction that needs no printables, only cardboard, tape, and paper). If you built a printer, you might need to obtain some or use slightly different methods for stage-0 bootstrap. 
    * If you live in an environment that is not so damp, or has less high humidity, you can use PLA for more construction
    * PLA is generally considered to have the safest fumes (though breathing them in is still highly undesirable) and less odor, so for stage-0 bootstrap it does work. 
    * Something to consider also is that PLA has a fairly low melting point (or at least, temperature at which it rapidly loses strength). Typically this is around 60-70°C, depending on the specific filament.
    * This renders PLA *unsuitable* for printing anything beyond a basic stage-0 ventilation chamber, because printer beds get hot enough that they can heat the chamber to these temperatures. PLA is hard to obtain in truly transparent forms, so it's not possible to see inside a chamber made of PLA without a webcam either. 
    * Its commonality in 3D printing is due to several reasons, but it is definitely the easiest material to print with. It's also available in by far the largest number of colours, so for parts which don't need to have much strength and don't rely on pliability much, it is a good material even in damp-ish conditions.
    * The colour availability is also important for aesthetic reasons. The things here almost certainly will be going in your living space/bedroom/etc. and so having a way to make sure things fit how you want your space to look is important.  
* PETG. This is a modified form of the material used to make things like water bottles, and it's an important material for this project. This is the easiest material to make transparent, and it creates prints which fail with warning (are not brittle), and has a much higher melting point than PLA. It is also very water resistant and watertight when printed (but it absorbs humidity in storage very well, which can affect printing due to water expansion during heating in the nozzle - however, it doesn't chemically damage the filament and it can be dried).
    * This is the material we will use for our stage-1 bootstrap ventilation chamber. 
    * PETG emits fumes when printing. They aren't horrific, but definitely aren't great either, so we want a stage-0 containment and ventilation system before printing it.
    * PETG is ideal for building physically strong infrastructure. ABS is also good for this, but not preferred due to fumes (and also it's harder to print without a heated chamber & ventilation/filtering system, that you really need to be properly sealed).
    * Ideally, you should obtain at least one roll of Clear PETG. This will allow for the construction of a transparent chamber, and PETG is undoubtedly the best way to build transparent objects, because it has such good self-adhesion that a high temperature and decent flow rate will probably be enough to ensure lack of air or moisture bubbles.
    * Printing transparent objects is somewhat challenging, so we will include guides for it and information from experiments on this front are highly encouraged. We want the chamber windows to be as good as possible for everyone. 
* TPU. A flexible filament, this is an important material to really push your 3D printing and infrastructure capabilities upward, because it lets you create flexible parts and importantly gasket-style seals. What this is extra useful for is mounting your 3D printer as a mobile thing inside a constructed storage system with mechanisms to connect and disconnect from ventilation when you move it while preserving sealed environments:
    * TPU is also what we use to convert a stage-1 ventilation chamber and system, into a stage-2 ventilation chamber, if you have access to it.
    * TPU emits dodgy fumes, hence the need for a stage-1 chamber before printing it. 
    * TPU filament will become damaged within a day or two of being exposed to humid environments (but printed TPU parts are very moisture resistant). 
    * TPU is an optional aspect of the setup, but very important if you want to create a properly sealed chamber & ventilation for printing things which emit dangerous fumes. TPU is chemical resistant in general as well, and can be used for other laboratory purposes (as long as your print is good). 

Filament usually comes on reels. Heuristically, as someone with both 500g and 1kg reels, I've found that the reels are approximately 19.5-20.5cm in diameter, so when building to scale for the filament reels, we probably want to use ~22cm as our width (for clearance). 

Also, it's best to mount filament above the printer and have filament directly hanging down, rather than off to the side, such that it rolls off the spool "naturally". This makes it far easier to print flexible filaments like TPU (and more generally, having the filament roll comfortably and smoothly is the most preferrable situation to avoid any filament stretching - the storage systems we construct will use specific mechanisms for this. 

Furthermore, when storing & using filament, make sure to follow the filament instructions to tie the end(s) of filament to the reel. Tangles are a nightmare, and yet almost trivial to prevent via doing that. 

When [slicing](#slicers) as well, you should add profiles for the specific temperatures specified by your filament (though we may push them). Most filament will have information on how to 3D print it effectively, and sometimes it varies between brands. 

### Important Notes on Humidity and Filament
Almost all 3D printer filaments are very vulnerable to exposure to high humidity air, as the humidity will infiltrate the filament (for PLA, it will actually degrade it chemically as well). 

This can make the filament more brittle (chemically, so even drying it won't fix the problem), and the water in the filament can cause steam, bubbles, or even printer damage if it's used in a 3D printer, because it gets heated to temperatures well above the boiling point of water.

As such, another priority of this project is constructing a filament storage system that can keep them airtight and easily allow the use of desiccant material - such as those silica gel bags you often get in electronics (or, in fact, 3D filament). Mitigations will be put in place even in stage 0 chamber construction, for keeping filament dry even when in use. 

This is one of the use cases for TPU, as we can create excellent flexible holes that preserve seals and yet can be used to move things (like hands, or connectors to the printer chamber) through while preserving the seals. 

I personally bought some very hefty silica gel bags (2x250g) and they were very affordable, but if you're extremely strapped for cash you can collect them from old electronics or food containers. They should be rechargeable in an oven set to a low temperature (so as not to damage the coating), or with some care in a microwave. 

3D printing filament is also usually vacuum sealed, and they have small silica bags inside that you can accumulate over time. Some filament reels actually come inside resealable bags as well, and until a proper storage system is printed you can use these for storage too as long as you keep the silica gel inside the bags. 

In a pinch - if you have not yet built a solid filament storage system but have some silica gel, you can store filament in a drawer or cardboard box or other box with some kind of lid with the silica pouches, if you have any space at all. This is particularly useful for the small bundles of filament you get with a lot of commercially-sold/mostly-prebuilt 3D printers. This isn't ideal but it should work as a temporary solution until we can build a reasonably effective storage system. Often, you can also reseal the bags which filament came in, and use their own little silica bags (though they have little capacity). 

## Slicers 
When 3D printing, you'll use a *slicer* to convert your 3D models into instructions that a printer can print - typically by turning them into layers. Slicers often let you assign different properties (e.g. speed, infill density, etc.) to different regions in a model, or make many copies of a model, or rescale and cut models. 

They also let you do infill - which reduces the amount of material in a big model by filling it with some hollow shapes, at the cost of strength and some other things but increasing printing time drastically.

Slicers usually have the notion of a *profile* - typically profiles for printers, a specific print type (e.g. fast, slow, layer thickness, material etc.), and for the filament itself. This is, at least, the structure in Prusa Slicer. In Orca Slicer, it's also similar (though it shares a software heritage. 

All 3D printers (for now) ultimately take their commands in the form of G-Code, which is also a more general CNC control mechanism. Slicers turn your model into G-Code that you can then send to the printer, essentially (though of course you can put more software that does more modifications in this process). This sounds simple, but slicers often let you do things like control flow rate, control infill, control the top/bottom layers of your print, control speed/precision/temperature, add text to your parts, etc.

The slicers I've had a look at are - for now - Prusa Slicer and [Orca Slicer](#notes-on-orca-slicer). The main FOSS slicer from a different heritage to PrusaSlicer, OrcaSlicer, Bambu Studio, and SuperSlicer (all slic3r based) is [cura-slicer], which the author is currently less familiar with. 

Over time as you upgrade or modify (or replace the firmware on) your printer, you'll need to make tweaked versions of printer profiles with your upgrades. PrusaSlicer and OrcaSlicer both make it easy to use an existing printer profile as a base and then make tweaks. Common tweaks include varying the type of nozzle (e.g switching from Brass+PTFE-lining into Stainless or Hardened Steel nozzles), and changing the firmware (typically, this'll involve installing Klipper, which is a likely thing to do when you want the most out of your printer).

### Notes on Orca Slicer
[OrcaSlicer][orca-slicer] is a fork of Bambu Slicer, which itself is a fork of PrusaSlicer. If installing Orca Slicer, we recommend not ticking the "Download Bambu Networking Plugin" box (the Bambu Networking Plugin is proprietary, and there are questions over whether this violates AGPLv3+, and, obviously, it sends stuff to Bambu). When it initializes, we recommend going into the down arrow on the *right* of the `File` menu, `> Preferences > Network > Stealth Mode` which disables any connection to the Bambu Cloud (I'm pretty sure that if you don't download the network plugin, it can't do this anyway from a cursory reading of the source, but I'm not entirely certain and I can't trace the code flow in the source easily - I'm not familiar with it's architecture).

OrcaSlicer also has some excellent [calibration tools](https://github.com/SoftFever/OrcaSlicer/wiki/Calibration). However, we recommend using these only once you are past [Stage 0](./bootstrap/stage/0.md) bootstrap as it requires printing a decent quantity of filament, which is probably not what you'll want to do until you have at least rudimentary ventilation. If you can wait until [Stage 1](/bootstrap/stage/1.md), with a more effective ventilation system with a place for a fan, it might be even better.

### Printer-Specific - Creality Ender 3V3 SE
In [PrusaSlicer][prusa-slicer], there is currently no profile for the Ender 3V3 (SE or KE version). For now, I'm using [this repository][prusa-slicer-ender-3v3-se] for the profiles. You'll want to run the Python script (it's only a few lines and easy to check) to generate the profile file, and import it into prusa via `File > Import > Import Config Bundle`. I find this ends up being very nice, compared to manually trying to import individual configs, and avoids weird menu stuff. This profile is quite conservative on print speed, and altering parameters is a part of the tuning process as we move further down bootstrap (though for now, the author is using Orca Slicer so someone else might have to mess with these, or you might need to wait for PrusaSlicer to incorporate information from OrcaSlicer into the default - not yet existing - profiles for the Ender 3V3SE/KE/etc.). 

In [OrcaSlicer][orca-slicer], the nightly version of the slicer contains relevant basic profiles for Creality Ender 3V3 SE.

The git repository of [cura-slicer] also seems to have a profile for the Ender 3V3 SE. I don't know if this is nightly-only or not at time of writing. 

## Printer Beds
When you 3D print something, it occurs on a printer bed. There are different types of printer bed. In [orca-slicer], you can also configure printer beds. An ideal printer bed optimises the adhesion of the printed material when printing, makes it easy to take off once the print has ended, and also can be heated. This is a function of the filament, of the bed material and texture, of the printing temperature of the first layer, as well as the temperature of the bed itself. That last factor can also affect the warping of any printed parts.

A common trick for 3D printers is to put gluestick (the kind of basic PVA stuff you'd use in school, don't use any other kind of harsher glue or you can damage the plate) under where your print will be, if prints get stuck to the bed too easily. This will dry out, but that's fine, it'll work much better because of it. 

(printer-specific: the default bed that comes with the Ender 3V3 SE its often useful to try this trick, as it's very strongly adhesive, though gets better when given time to cool - it's worth trying lower bed temperatures too though, and OrcaSlicer's profile doesn't really need it at least when working with PLA because they use a much lower bed temperature). 

Reducing over-adhesion when the print ends can be done in some different ways as well, like decreasing the bed temperature or using different (potentially smoother) materials. Under-adhesion during printing can be resolved in similar but reverse ways.

A common material for print beds is something called [PEI](https://reprap.org/wiki/PEI_build_surface). It's very easy to find pre-sold PEI beds, though RepRap provides a way to make them yourself :) - if you do buy them, consider that the "official" build area of some printers is smaller than the full plate as they have small parts of the plate given over to clearance as well as wiping off excess extrusion from the nozzle. Measure the actual plate that comes with your printer. Also - most printers use magnets to hold the surface in place, which makes for much easier handling, so it's worth finding coated iron/steel surfaces in particular.

Some people also have a decent time printing on other materials. See [the reprap page on bed materials](https://reprap.org/wiki/Bed_material) for a full listing, though the consensus on that page seems to be PEI. For heated beds (which is almost every modern printer, though Hyper-Extreme-Budget ones might not), RepRap has info [here](https://reprap.org/wiki/Heated_Bed#Surface_Materials)


[cura-slicer]: https://github.com/Ultimaker/Cura "Cura Slicer"
[orca-slicer]: https://github.com/SoftFever/OrcaSlicer "OrcaSlicer"
[prusa-slicer]: https://www.prusa3d.com/en/page/prusaslicer_424/ 
[prusa-slicer-ender-3v3-se]: https://github.com/suchmememanyskill/PrusaSlicer-Ender3-v3-SE-Config/tree/main?tab=readme-ov-file
