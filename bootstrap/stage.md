# Bootstrap Stages - An Overview
This file is an overview of the various bootstrap stages and what they're for. The bootstrap process is initially designed to create improving ventilation/filtration and containment capabilities for the printer (as well as storage and similar). Each stage depends on the last. 

Stages may have one set of instructions, but contributors can also contribute multiple ways of doing things (either entirely separate paths, or different versions for small parts of it). 

Stages usually incorporate documentation as well as a collection of components (involving a top-level variable-unifying `scad` file plus scad files in subdirectories for the various parts). 

## Stage 0 
This is the stage when you have a 3D printer, cardboard, and presumably some scraps and the odd cutting tool, and that's it. From this, we want to build a stage-0 chamber and ventilation system. The documentation for this stage can be found [here](./stage/0.md), 

## Stage 1
This is the stage where we have a stage-0 ventilation system and we'll use it to print various PETG components (some of which need to be printed in a certain way for transparency) to build the actual printer chamber (stage-1 chamber) and ventilation system. This stage also makes allowances for cable management addons in the ventilation system. The documentation for this stage can be found [here](./stage/1.md). This is the point where we can construct a decent filament storage system (stage-1 filament storage). This is the point at which modular, fractal storage can be constructed as we now have the ability to print PETG safely with a chamber that is actually built not to fall apart if touched wrong - though a Stage-2 chamber is superior still. 

## Stage 2
This is the stage where we use the stage-1 chamber to print components out of TPU filament that allow converting the stage 1 chamber into a stage 2 (fully sealed) chamber, which also enables components to make the ventilation system itself more sealed, and similar things like stage-2 sealed filament storage. The documentation for this stage can be found [here](./stage/2.md) 
