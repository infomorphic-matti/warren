// This is the central component that allows plugging in edge-components to hold cardboard 
// sheets together with some rigidity. The also have structured holes on both ends to allow 
// joints to click them together using compliant mechanisms. These holes are rhomboids. 
//
// The entire system is based on compliant "clip"-like joints. 
//
// The rods themselves are best printed upright, this allows for easy joint construction without overhangs. The overall 
// "component" is the `combiner_rod` module, which takes 2 main parameters:
// * length - how long the rod should actually be.
// * cardboard_sheet_thickness - the thickness of the cardboard in mm (default: 6). 
//   This gets slightly "compressed" in the actual object to hold the cardboard.
// * boundary_thickness - thickness to make the actual rod boundary in mm (default: 0.8mm)
//   this also controls the thickness of the cardboard-clips
// * clip_extent - length of the outward clips on the edges of each face in mm (default: 10)
// * sides - indicate which sides should have the clip extensions. 
//    This is a 4-element list of booleans (like [false, true, true, false]), each one indicating a 
//    side - first is +x, second is +y, third is -x, fourth is -y
//    Default is [true, true, true, true]
//
// The centre of the holes on the ends are inset 8mm (to centre) from the actual ends of 
// the rod. These holes are shaped with an along-rod length of 4mm and an across-perimeter 
// length of 2.5mm, which should avoid any overhang issues while printing, as the holes are 
// rhomboid.
//
// There is also a trench of initial depth 0.2mm carved into the inside of the object, which 
// is of width 2.5mm. This trench goes all the way to the rhomboid hole from each end of 
// the rod, and exists to help slide any connectors in, and as it approaches the locking 
// hole, it approaches a point where the thickness of the edge material is only 0.2mm 
// (though with limitations of the manufacturing process to be considered). 
//
// Connectors should have angled-to-the-rod-centre connector points to ease insertion and 
// removal (it should slope down on the towards-rod direction - away-from-rod direction
// is covered by the inner slope), and should make sure to have the locking parts a bit 
// thicker than  `boundary_thickness` so users can press them and slide them back out 
// easily. 
//
// Connector implementations should probably avoid using compliant connectors in more than 
// one or two of the sides, so they can be easily disassembled. They should however make 
// sure to cover any of the other holes on the inside (without any compliant mechanism).

epsilon = 0.001;


function compressed_cardboard_thickness(cardboard_sheet_thickness) = 
    cardboard_sheet_thickness - 1;
// The side length of the core rod component (not including the "wings"). Half of this gives
// the shift outwards along each axes that the edges of the core rod is at.
function central_rod_sidelength(cardboard_sheet_thickness, boundary_thickness) = 
    compressed_cardboard_thickness(cardboard_sheet_thickness) + 2 * boundary_thickness;

// Create one set of side-clips on the positive x axis and only in the positive y direction. 
module combiner_rod_single_side_clip(length, cardboard_sheet_thickness, boundary_thickness, clip_extent) {
    y_shift_for_duplication = compressed_cardboard_thickness(cardboard_sheet_thickness) / 2;
    // Amount to shift in the x-axis to give space for the core rod when this output gets mirrored.
    core_rod_shift = central_rod_sidelength(cardboard_sheet_thickness, boundary_thickness) / 2;
    translation = [core_rod_shift, y_shift_for_duplication - epsilon, 0];
    translate(translation)
        cube(size = [clip_extent, boundary_thickness, length], center = false);
    mirror([0, 1, 0])
        translate(translation)
            cube(size = [clip_extent, boundary_thickness, length], center = false);
}


// Create a connector hole and trench object at the correct height and distance in the 
// positive x side.
//
// This is only on the bottom of the z-vertical object, but can be mirrored and rotated 
// before subtraction. 
module combiner_rod_single_connector_hole(cardboard_sheet_thickness, boundary_thickness) {    
    connector_hole_centre_inset = 8;
    connector_hole_width = 2.5;
    connector_hole_height = 4;

    hole_x_shift = central_rod_sidelength(cardboard_sheet_thickness, boundary_thickness)/2 - boundary_thickness/2;

    // Create an extruded polygon of the correct shape, but at z-centre, with the longer 
    // part of the object along the x-axis (so it can easily be flipped, rotated, then 
    // translated up). 
    //
    // This is the hole
    translate([hole_x_shift + epsilon /2, 0, connector_hole_centre_inset]) 
    rotate(90, [0, 1, 0]) 
        linear_extrude(height = boundary_thickness + epsilon, center = true)
            polygon([
                0.5 * [-connector_hole_height, 0],
                0.5 * [0, -connector_hole_width],
                0.5 * [connector_hole_height, 0],
                0.5 * [0, connector_hole_width]
            ]);

    // This is the depth of the trench at the edge of the rod
    trench_start_depth = 0.2;

    // This is the thickness of the remaining side at the actual hole. (or, well, the centre
    // of the hole)
    trench_end_side_thickness = 0.2;

    // We create the required shape by doing a vector-scaled linear extrusion with a vector 
    // scale factor taking a (trench_width x trench_start_depth) square into a 
    // (trench width x (boundary_thickness - trench_end_side_thickness)) square,
    // with the variation occuring in the x-side of the square as we are going to 
    // create the object in the proper location on the +x side of the rod.
    trench_x_scale_factor = (boundary_thickness - trench_end_side_thickness)/trench_start_depth; 

    // The trench is not centred in the x, so it's entirely in +x and needs to avoid the
    // full boundary thickness such as to not move outside the core rod.
    trench_x_shift = central_rod_sidelength(cardboard_sheet_thickness, boundary_thickness)/2 - boundary_thickness;

    translate([trench_x_shift - epsilon, 0, -epsilon/2])
        linear_extrude(
            height = connector_hole_centre_inset, 
            scale = [trench_x_scale_factor, 1], 
            center = false
        ) translate([0, -connector_hole_width / 2])
                square(size = [trench_start_depth, connector_hole_width], center = false);
}

// Create the connector holes for both ends on the +x side of the rod (for later deletion).
module combiner_rod_connector_hole_pair(length, cardboard_sheet_thickness, boundary_thickness) {
    // mirror the object around the XY plane at z = length/2 by doing a translation, doing a mirror
    // then doing an inverse translation. basically an "at" operation.
    translate([0, 0, length/2])
    mirror([0, 0, 1])
    translate([0, 0, -length/2])
    combiner_rod_single_connector_hole(cardboard_sheet_thickness, boundary_thickness);
    // Normal one at the bottom of the z-axis
    combiner_rod_single_connector_hole(cardboard_sheet_thickness, boundary_thickness);
} 

// Module that takes a `sides` parameter and rotates it's children
//
// `sides` is a 4-element boolean array
module combiner_rod_rotate_axes(sides) {
    // inclusive ranges :/
    for (i = [0:1:3]) {
        if (sides[i]) {
            rotate(90 * i, [0, 0, 1])
                children();
        }
    };
} 

module combiner_rod_core(length, cardboard_sheet_thickness, boundary_thickness) {
    // 2* is for both of the edges
    outer_cuboid_sides = compressed_cardboard_thickness(cardboard_sheet_thickness) + 2 * boundary_thickness;
    inner_cuboid_sides = compressed_cardboard_thickness(cardboard_sheet_thickness);
    translate([0, 0, length/2])
        difference() {
            cube(size = [outer_cuboid_sides, outer_cuboid_sides, length], center = true);
            cube(size = [inner_cuboid_sides, inner_cuboid_sides, length + epsilon], center = true);
        }
}

module combiner_rod(
    length, 
    cardboard_sheet_thickness = 6, 
    boundary_thickness = 0.8, 
    clip_extent = 10, 
    sides = [true, true, true, true]
) {
    difference() {
        union() {
            // extent of the clippable areas 
            combiner_rod_rotate_axes(sides) 
                combiner_rod_single_side_clip(
                    length = length, 
                    cardboard_sheet_thickness = cardboard_sheet_thickness, 
                    boundary_thickness = boundary_thickness, 
                    clip_extent = clip_extent 
                );

            combiner_rod_core(
                length = length, 
                cardboard_sheet_thickness = cardboard_sheet_thickness,
                boundary_thickness = boundary_thickness
            );
        }
        

        combiner_rod_rotate_axes([true, true, true, true])
            combiner_rod_connector_hole_pair(
                length = length, 
                cardboard_sheet_thickness = cardboard_sheet_thickness,
                boundary_thickness = boundary_thickness
            );
    }
}

combiner_rod(length = 30, sides = [true, true, false, false]);

