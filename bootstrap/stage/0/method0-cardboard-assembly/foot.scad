// This module is designed to create stabilising feet for a cardboard-based ventilation chamber. 
//
// All measurements are in mm.
//
// Params:
// * lengths - 4-element array of how long you want the supports to go in the [+x, +y, -x, -y] axes. For example, for a corner-support,
//   you'd want something like [50, 50, 0, 0], and for a support along an edge, [50, 0, 50, 0] would be a good amount.
//   Default is [50, 50, 50, 50] (you'll want to alter this - this is a 4-way junction and probably not what you want lol)
//   Note that this length includes the central join-point but not the little bit on the opposite side of the axis. 
// * cardboard_thickness - 4-element array of how thick the cardboard is in each axis [+x, +y, -x, -y]. Default values are `6` in all 
//   axes. 1mm is subtracted from these to provide friction holds, but if your cardboard is compressed along one of these axes, then you
//   probably want to reduce the amount. 
// * wall_thickness - The actual thickness of the support walls. Default value is 0.8mm 
// * axis_height - 4 element array of the height of the support fins in each axis ([+x, +y, -x, -y]). The default values are [20, 20, 20, 20].
//   You shouldn't need to put this too high for rigidity, as there are other means of providing rigidity available. 
// * core_height - The height of the core part of the foot. This is the point where all the supports join, and allows you to create more rigidity
//   if you're placing the foot over a connection between two or more sheets you can provide more rigidity.
//   Default value: 40mm or 4cm. This is globbed as "part of" the axis height, so if it's 
//   the same as the axis height then it won't produce any fins. 
// * core_fins - The length of the fins from the foot core in each axis as a 4-dimensional array. 
//   Default values are [15, 15, 15, 15]. If the corresponding support length for the axis is zero, though, 
//   no fins will be generated. 
// * support_base_extend - 4 element array of pairs of numbers that provides the extension 
//   amount of a support sheet beyond just "below the middle" for each of the axes, on each of the
//   left and right side of the axis - format is (left, right). Default [[15, 15], [15, 15], [15, 15], [15, 15]]
//   Disabling the extra support is as easy as setting the left or right value to zero, and 
//   this setup is very useful e.g. to only provide support feet on the outside or inside of 
//   a corner. 

// Construction Mechanism of Object
//
// The easiest way to construct this object is by selective cutting of the cardboard support 
// areas, making sure to sequence it so holes appear appropriately. 
epsilon = 0.001;

// cardboard thickness but compressed.
function compressed_cardboard_thickness(cardboard_thickness) = cardboard_thickness * 0.75;

// Thickness of a horizontal fin that includes the walls
function fin_horizontal_thickness(cardboard_thickness, wall_thickness) = 
    compressed_cardboard_thickness(cardboard_thickness) + 2 * wall_thickness; 


// Construct the support feet for a segment with the given fin horizontal thickness.
//
// This includes the support feet for the central point. 
//
// support_base_extend is the standard (left, right) notation. 
module horizontal_foot_solid(
    length, 
    fin_horizontal_thickness, 
    wall_thickness, 
    support_base_extend
) {
    total_support_width = fin_horizontal_thickness + support_base_extend[0] + support_base_extend[1];
    // Shift in the y-axis to make the support base position correctly w.r.t the main fin.
    // The `[1]` ensure that the first value is "left" side, and second is "right" side
    // because -Y is on the "right" of +x and hence shifting in that direction to a beyond-fin
    // length of support_base_extend[1] ensures that that one is the size on the right.
    total_centering_shift = -(fin_horizontal_thickness/2 + support_base_extend[1]);

    // Main support cuboid
    translate([0, total_centering_shift, 0])
    cube([length, total_support_width, wall_thickness]);

    // The coordinate pairs to make the foot act like it is taking an anticlockwise corner,
    // such that the inside corner extends as-per "left", and the outside corner extends 
    // as-per "right"
    anticlockwise_corner_matching_coordinates = let (
        l = support_base_extend[0] + fin_horizontal_thickness /2, 
        r = support_base_extend[1] + fin_horizontal_thickness/2
    ) [
        [l, l],
        [l, -r],
        [-r, -r],
        [-r, l]
    ];

    // Support cuboid "squaring out" the inner coordinates.
    linear_extrude(height = wall_thickness) 
    polygon(anticlockwise_corner_matching_coordinates);
}

// Create the horizontal extension solid component facing out along the positive x-axis.
// This by nature includes the support walls (i.e. their thickness). 
//
// There is also a small component in the negative x axis, which is designed for being unioned
// with a bunch of other combined bits. 
//
// support_base_extend is the [left, right] pair. 
//
// This includes `horizontal_foot_solid`.
module horizontal_extension_solid(
    length, 
    cardboard_thickness, 
    wall_thickness, 
    support_fin_height, 
    support_base_extend
) {
    fin_thickness = fin_horizontal_thickness(cardboard_thickness, wall_thickness); 
    // Fin height including the base wall.
    inclusive_height = support_fin_height + wall_thickness;

    // Create a cube with both the space at the sides to have the support fins, 
    // and the space at the bottom for the support wall
    //
    // This is the main fin
    translate([0, -fin_thickness/2, 0]) 
    cube(
        size = [
            length, 
            fin_thickness, 
            inclusive_height
        ], 
        center = false
    );

    // This is the central fin that contributes to the core
    translate([-fin_thickness/2, -fin_thickness/2, 0])
    cube(
        size = [fin_thickness, fin_thickness, inclusive_height],
        center = false
    );

    horizontal_foot_solid(
        length = length,
        fin_horizontal_thickness = fin_thickness,
        wall_thickness = wall_thickness,
        support_base_extend = support_base_extend
    );
} 

// Construct all the horizontal extension solids for each axis, properly rotated and 
// unioned.
//
// Same parameters as horizontal_extension_solid, except that relevant parameters 
// are replaced with 4-element arrays, each one for [+x, +y, -x, -y], and `length` is 
// pluralized to `lengths`. 
module all_horizontal_extension_solid(
    lengths, 
    cardboard_thickness, 
    wall_thickness, 
    support_fin_height, 
    support_base_extend
) {
    // 1-based lengths u.u
    for (quadrant = [0:1:3]) {
        rotate(a = quadrant * 90, v = [0, 0, 1])
            horizontal_extension_solid(            
                length = lengths[quadrant], 
                cardboard_thickness = cardboard_thickness[quadrant], 
                wall_thickness = wall_thickness, 
                support_fin_height = support_fin_height[quadrant], 
                support_base_extend = support_base_extend[quadrant]
            );
    }
}

// Like `horizontal_extension_solid` but creates the object to be deleted instead...
//
// This also accounts for the case where `length` is <= zero and avoids some deletions.
// in that case.
//
// Note - you must provide `max_height` rather than just `support_fin_height`.
// This is so that in the center there are always no walls when different axes have different
// support fin heights, and also allows deleting the core components 
module horizontal_extension_hole(
    length,
    cardboard_thickness,
    wall_thickness,
    max_height, 
) {

    if (length > 0) {
        fin_thickness = fin_horizontal_thickness(cardboard_thickness, wall_thickness);
        deletion_thickness = fin_thickness - 2 * wall_thickness;
        // Deletion for the main part of the fin. 
        // That doesn't include the wall at the bottom
        translate([0, -deletion_thickness/2, wall_thickness])
            cube([length + epsilon, deletion_thickness, max_height + epsilon]);

        // Deletion for the central core. 
        translate([0, 0, wall_thickness + max_height/2])
            cube(
                size = [deletion_thickness, deletion_thickness, max_height + epsilon], 
                center = true
            );
    }
}

// Like `horizontal_extension_hole` but creates all the appropriate holes in each 
// axis, rotated as proper, to be deleted.
//
// Params are the same but the ones which can be different are turned into 4-element 
// arrays ([+x, +y, -x, -y]), and `length` is pluralized to `lengths`
//
// `max_height` is also automatically calculated from the `support_fin_height`
// array instead of passed directly, but you also can pass a secondary max_height 
// included in the list.
module all_horizontal_extension_hole(
    lengths,
    cardboard_thickness,
    wall_thickness,
    support_fin_height,
    max_height = 0,
) {
    max_height = max(max(support_fin_height), max_height);
    // 1-based lengths u.u
    for (quadrant = [0:1:3]) {
        rotate(a = quadrant * 90, v = [0, 0, 1])
            horizontal_extension_hole(            
                length = lengths[quadrant], 
                cardboard_thickness = cardboard_thickness[quadrant], 
                wall_thickness = wall_thickness, 
                max_height = max_height 
            );
    }
}

// Making space in the core support fins is basically just done by the 
// horizontal holes. 
//
// This function creates the solid component of the core rigidity supports in 
// the +x axis. 
//
// Params:
// * `enable` - this is expected to be `true` if `length` > 0 for the axis, and `false` otherwise
//   it controls whether the core rigidity mechanism extends to that axis, or if a simple 
//   wall extension upwards is what's needed.
// * `core_height` - the actual height of the rigidity fins, starting from the base of the object.
// * `core_fins` - the length of the core rigidity fins for this axis, 
// * `wall_thickness` - thickness of walls.
// * `cardboard_thickness` - thickness of the cardboard across this axis. 
//
// The main thing we need to do is create a solid core that extends back through 
// the centre to match the way the horizontal fins are created. The horizontal holes
// will do the rest for us
//
// This function is only in the +x axis, and can be improved by rotation. In practice, we 
// actually end up calling out to the horizontal solid module, because the vertical support 
// fins are essentially equivalent to one that is really short but really tall. Also 
// totally lacks a support base because that would be pointless.
module vertical_core(
    enable,
    core_height,
    core_fins,
    wall_thickness,
    cardboard_thickness
) {
    horizontal_extension_length = enable ? core_fins : 0;
    horizontal_extension_solid(
        length = horizontal_extension_length, 
        cardboard_thickness = cardboard_thickness, 
        wall_thickness = wall_thickness, 
        support_fin_height = core_height, 
        support_base_extend = [0, 0]
    );
}

// Create all vertical support fin cores (the removal is done by the horizontal holes). 
//
// This takes lengths and automatically calculates `enable` from it. It also ensures that
// the core fin length can't be longer than the horizontal support length (to avoid big
// blocks without deletion). 
//
// Other than that, the parameters are the same but where appropriate they are turned into
// 4-element arrays and rotated. The arrays are [+x, +y, -x, -y].
module all_vertical_core(
    lengths,
    core_height,
    core_fins,
    wall_thickness,
    cardboard_thickness,
) { 
    // 1-based lengths u.u
    for (quadrant = [0:1:3]) {
        enable = lengths[quadrant] > 0;
        rotate(a = quadrant * 90, v = [0, 0, 1]) 
        vertical_core(
            enable = enable,
            core_height = core_height,
            core_fins = min(core_fins[quadrant], lengths[quadrant]),
            wall_thickness = wall_thickness,
            cardboard_thickness = cardboard_thickness[quadrant]
        );
    }
}

module foot(
    lengths = [50, 50, 50, 50],
    cardboard_thickness = [6, 6, 6, 6],
    wall_thickness = 0.8,
    axis_height = [20, 20, 20, 20],
    core_height = 40,
    core_fins = [15, 15, 15, 15],
    support_base_extend = [
        [15, 15],
        [15, 15],
        [15, 15],
        [15, 15],
    ]
) {
    difference() {
        union() {
            all_horizontal_extension_solid(
                lengths = lengths,
                cardboard_thickness = cardboard_thickness,
                wall_thickness = wall_thickness,
                support_fin_height = axis_height,
                support_base_extend = support_base_extend
            );

            all_vertical_core(
                lengths = lengths,
                core_height = core_height,
                core_fins = core_fins,
                wall_thickness = wall_thickness,
                cardboard_thickness = cardboard_thickness
            );
        }

        all_horizontal_extension_hole(
            lengths = lengths,
            cardboard_thickness = cardboard_thickness,
            wall_thickness = wall_thickness,
            support_fin_height = axis_height,
            max_height = core_height
        );
    }
}

foot();
