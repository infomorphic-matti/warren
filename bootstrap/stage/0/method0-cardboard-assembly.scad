// This is a module providing pre-configured parts from the library of actual parts.
//
// Set variables and then use `include`. This will import various modules into your namespace
// with common variables when needed, and you can pick and choose from them.
//
// All lengths are in mm.

use <method0-cardboard-assembly/foot.scad>

// Thickness of actual support walls
wall_thickness = 0.8;

// Default thickness of cardboard
cardboard_thickness = 6;

// Default extent length for joints - 50mm
default_extent_length = 50;

// Default vertical extent length for joints - default is `default_extent_length + 30` 
default_vertical_extent_length = default_extent_length + 30;

// Default "support fin"/"holding fin" length for joints - 20mm
default_fin_length = 20; 

// Default "support base" extent on the *outside* for joints that act as feet.
// 20mm.
default_outside_support_base = 20;

// Default "support_base" extent on the *inside* for joints that act as feet.
// 10mm
default_inside_support_base = 10;

// Default "support base" extent when there isn't a clear inside/outside (average of
// default outside & inside)
default_unknown_support_base = (default_outside_support_base + default_inside_support_base)/2;

/// Repeat the same value, `v`,  `n` times into a vector
function vrepeat(v, n) = [for(i = [0:n-1]) v];

/// Select values from a vector using a boolean vector of the same size, producing zero if 
/// the bools are false.
function vselect(vector, boolean_vector) = [for(i = [0:(len(vector) - 1)]) boolean_vector[i] ? vector[i] : 0];

/// Create a vector from the boolean vector by filling in the value `t` when the boolean vector
/// contains `true`, and filling in the value `f` when it contains `false` 
function vboolmap(boolean_vector, t, f) = [for(c = boolean_vector) c ? t : f];

// Almost fully custom foot. 
// Main thing that isn't controlled is wall_thickness and axis fins/core fins size (though 
// core height is still controlled).
//
// See the `foot.scad` module for info on how this works. 
module foot_custom_all(
    lengths = [default_extent_length, default_extent_length, default_extent_length, default_extent_length],
    cardboard_thickness = [cardboard_thickness, cardboard_thickness, cardboard_thickness, cardboard_thickness],
    core_height = default_vertical_extent_length,
    support_base_extend = [
        [default_unknown_support_base, default_unknown_support_base],
        [default_unknown_support_base, default_unknown_support_base],
        [default_unknown_support_base, default_unknown_support_base],
        [default_unknown_support_base, default_unknown_support_base]
    ]
) {
    uniform_fins = let (df = default_fin_length) [df, df, df, df];
    foot(
        lengths = lengths,
        cardboard_thickness = cardboard_thickness,
        wall_thickness = wall_thickness,
        axis_height = uniform_fins,
        core_height = core_height,
        core_fins = uniform_fins,
        support_base_extend = support_base_extend
    );
}

/// Conditionally enable/disable the connectors on each axis and the rigidity core. 
/// Params:
/// * `enable_axes` - list of 4 booleans, default is [true, true, true, true]. This 
///    controls whether or not you want to enable the [+x, +y, -x, -y] axis. 
/// * `enable_core` - boolean on whether or not you want to enable the supporting core.
//    default is true. 
module foot_custom_conditional(
    enable_axes = [true, true, true, true],
    enable_core = true
) {
    foot_custom_all(
        lengths = vboolmap(enable_axes, default_extent_length, 0),
        core_height = enable_core ? default_vertical_extent_length : 0
    );
}

/// Create a foot right-angle-corner. 
///
/// This allows you to toggle the core rigidity component as well as alter the 
/// cardboard width in the left and right corner (looking from the center out into 
/// the right angle). 
/// If you have two different cardboard thicknesses in your setup, remember to check that
/// you've gotten the orientation correct - the base means that the corners are chirally 
/// distinct.
module foot_corner(
    left_cardboard_thickness = cardboard_thickness,
    right_cardboard_thickness = cardboard_thickness,
    enable_core = true
) {
    foot_custom_all(
        lengths = [default_extent_length, default_extent_length, 0, 0],
        core_height = enable_core ? default_vertical_extent_length : 0,
        cardboard_thickness = [right_cardboard_thickness, left_cardboard_thickness, 0, 0],
        support_base_extend = [
            [default_inside_support_base, default_outside_support_base],
            [default_outside_support_base, default_inside_support_base],
            // Irrelevant because there are no horizontal extents. 
            [0, 0],
            [0, 0]
        ]
    );     
}

/// Create a foot for a straight-line joint. This allows you to set the two cardboard 
/// thicknesses, and whether or not to use the rigidity core.
/// 
/// If you have distinct inner and outer support base sizes, pay attention to the order 
/// of your cardboard thicknesses, as the inner and outer support bases are on fixed sides
/// of the joint/foot. If the thicknesses are all the same, it obviously doesn't matter, 
/// but if they're not, you probably want to match them up with the in/out bases.
module foot_line(
    front_cardboard_thickness = cardboard_thickness,
    back_cardboard_thickness = cardboard_thickness,
    enable_core = true
) {
    foot_custom_all(
        lengths = [default_extent_length, 0, default_extent_length, 0],
        core_height = enable_core ? default_vertical_extent_length : 0,
        cardboard_thickness = [front_cardboard_thickness, 0, back_cardboard_thickness, 0], 
        support_base_extend = [
            [default_inside_support_base, default_outside_support_base],
            // Irrelevant because there are no horizontal extents. 
            [0, 0],
            [default_outside_support_base, default_inside_support_base],
            // Irrelevant because there are no horizontal extents. 
            [0, 0]
        ]
    );
}

/// Create a foot for a T-joint. This allows you to set the 3 cardboard thicknesses and
/// control the presence/absense of the rigid core. The corner bits are alwaus considered 
/// "inside" for the purposes of support bases.
/// 
/// `left`, `middle`, and `right` refer to the positions of the support fins when looking
/// out from the middle of the junction towards the middle T-junction support.
module foot_t_junction(
    left_cardboard_thickness = cardboard_thickness,
    middle_cardboard_thickness = cardboard_thickness,
    right_cardboard_thickness = cardboard_thickness,
    enable_core = true
) {
    
    foot_custom_all(
        lengths = [default_extent_length, default_extent_length, 0, default_extent_length],
        core_height = enable_core ? default_vertical_extent_length : 0,
        cardboard_thickness = [middle_cardboard_thickness, left_cardboard_thickness, 0, right_cardboard_thickness], 
        support_base_extend = [
            [default_inside_support_base, default_inside_support_base],
            [default_outside_support_base, default_inside_support_base],
            // Irrelevant because there are no horizontal extents. 
            [0, 0],
            [default_inside_support_base, default_outside_support_base]
        ]
    );
} 
