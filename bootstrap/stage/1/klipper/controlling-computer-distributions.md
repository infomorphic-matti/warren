# Distributions For Your Klipper Controlling Computer
This document is designed to help you pick a distribution based on the hardware you intend to install the Klipper daemon (+ Moonraker, + maybe other services) on, and provide tips to help configuring them for secure access before the first power-on, plus provide information about other security possibilities.

## Security Options
Security options are to some degree dependent on the threat model. We want users to always at the very least ensure that any *remote* access (i.e. SSH) is always authenticated via public keys from the very first boot, and always disallow remote password attempts. 

There are other, slightly more stringent security options you can implement as well, dependent upon your threat model. 

The basic idea of setting up PKA SSH is to mount the flashed image of the distribution you want to run, and then do the following:
* Find the home directory of the "default" user of the system you want to modify, and create the file `~/.ssh/authorized_keys` for that user, by putting lines in the format produced by `ssh-add -L` into that file. This may be the root user directly, but many systems (like Raspberry Pi OS) will have their own "default" user, which should be preferred.
    * Make sure the file and containing directory is globally readable.
    * Some modern distributions no longer have a default user - instead, you can set up a user by providing a file with the username and (hashed) password which will then initiate the user on first boot. This may cause issues with setting up Public Key Authentication before first boot, depending on the mechanism. 
    * If the mechanism is based on "transferring" from a default user (as it is in Raspberry Pis) it should still work, as long as the home directory containing your SSH `authorized_keys` configuration is moved during the rename (the important thing to look for is a `usermod` command with the `-m` flag). 
* Ensure that `/etc/ssh/sshd_config` has the setting `PasswordAuthentication no`
* Make sure that the distribution actually enables the `sshd` server on first boot. This is usually the part that's distribution-dependent. Some will always enable it, but others might need something like a file, or manually enabling the service. 
* Make sure that the distribution *doesn't* override or otherwise delete the configuration you just created. Most probably, it won't, but this could be an issue.

A lot of distributions also explicitly provide a means to set up PKA SSH directly in a headless install. Then all you need to do is disable Password Login in the SSH configuration on the image (this might also be configurable via an "official" mechanism, and if it is, you should also use that).

If you just wanted the normal Public-Key Authenticated SSH, [skip to the distribution options](#distribution-options). However, we also recommend making a decision about [the password of the user account](#security-options-strong-password-or-account-locking-recommended).

### Security Options - Strong Password or Account Locking (recommended).
Default passwords for most embedded distributions are usually very weak (or don't exist). Even if you have prevented remote access to your machine without a public key, someone could still just walk up to your controlling computer, log in, and alter things. 

This would not, for instance, induce a reboot of the system or some other similar issue (like if the SD Card was removed - even if it was not encrypted and could be read and modified freely, just pulling it out would usually provide some indication of an issue or something).

More modern systems often will let you provide a file before first boot with a username and (hashed) password. For the case where you setup a "strong password", you can use this to create a user with better security, and it avoids you needing to change the password later. See [the instructions for SSH Public-Key Authentication](#security-options) for how this affects SSH config. It's also worth making sure the root account doesn't have some sort of laughably insecure default credentials, which would waste all the effort that has gone into the user configuration system.

There are two main options to solve this:
* Set a proper strong password for the user account (preferably some long random string of letters and numbers). 
    * This is an option you can take just fine, but it means another password to either remember (if you care to, you might just not do that), or put it in a password manager (which is annoying to use with CLI SSH). 
    * This ensures that if you use `sudo`, you must enter a password. This may or may not be necessary depending on your threat model - after all, you're already authenticated and logged in by presumably SSH private key. 
    * This provides some rudimentary protection against compromised private keys, though.
* Lock the user account to only be accessible via SSH (i.e. disable password access/"lock the password"):
    * This is more convenient, and avoids the need to store and/or remember a long password.
    * However, this requires configuring `sudo` to allow the user in question to run commands without providing a password. This may be acceptable in a lot of cases, as you're already authenticated by SSH.
    * This loosens the protection from, specifically, a compromise of your private SSH key. 

These steps don't need to be applied before first boot - as long as the Pi is physically in front of you, you know no-one is going to be messing around with it, so you can ssh in and perform the required modifications (before rebooting). 

Sometimes the default user is just `root`. In that case, the `sudo` parts of the following instructions are not relevant. When editing `sudo` configuration files, you should always use `visudo` - this program will check for syntax errors before modifying the "real" file, as messing it up is a massive pain. 

Something to note is that `sudo` will match inside `sudoers` based on the *last* directive, even if that directive is (1) more restrictive than a previous one in terms of things like `NOPASSWD` and/or (2) more general in the users it affects (e.g. selecting an entire group rather than an individual user. This problem is mostly mitigated by placing your custom modifications inside `/etc/sudoers.d/`, inside a file that has a "high" alphanumeric ordering (i.e. prefix it like `090-` or something so it comes after all the other files in that directory). 

Setting a long password is easy:
* Login over SSH using your public key - into whatever user was defined (default, user-configuration-like, root)
* Set the password via `passwd` (literally just run that command).  
* You should check inside both the `/etc/sudoers` file and any files in `/etc/sudoers.d/`, to make sure there are no directives that tell `sudo` not to require a password (on RPi, there is one in one of the config files, and you need to remove the `NOPASSWD: ` part to get any benefit out of the password you just set). 
* As a consideration, if the root account exists (is enabled) and the default user is not root, it's worth making sure that the root user has a secure password - if not, you can also do things like lock or delete the root account, or give it a strong password.

Locking the password is also relatively simple. However, you must make sure to modify `sudo` directives BEFORE locking your password, otherwise you'll be unable to do any admin tasks after your modification without some wrangling. To do it:
* Login over SSH using your public key.
* Check your `/etc/sudoers` file, and any files in `/etc/sudoers.d/`, to see if there are directives disabling password requirements for your user or a group they are in. The *last* statement that applies to your user (either directly, or via group- or alias- matching) determines this - see if you see something like `(ALL) NOPASSWD: ALL`). If a statement like this applies, then you're already done with the `sudoers` configuration step. 
* If such a statement does not exist, then create a file with a name like `090-user-<default-username>-nopasswd` or something, and put the line `<your user>= (ALL) NOPASSWD: ALL` in that file *using `visudo`*. This says `<your user>` can run `ALL` commands without a password or other auth, pretending to be `(ALL)` other users.
* Now you know you can access `sudo` without a password for the relevant user, you can lock the password of the current user via `passwd -l`
* As a consideration, if the root account exists (is enabled) and the default user is not root, it's worth making sure that the root user has a secure password - if not, you can also do things like lock or delete the root account, or give it a strong password. 

If you don't want the much more technical security measure(s) described further below this one, [skip to distribution options](#distribution-options).

### Security Options - Full-Disk Encryption of Operating System
It should be possible using either the `initramfs` system (for distributions that use that for booting) or hooks in the `initrd` system (for distributions that use that for booting, like RasPiOS), plus LUKS tooling, to provide full-disk-encryption for the operating system and data.

You can make it so that this requires a keyboard on each boot to enter a password or other key, or you can use a `dropbear` SSH hook to allow SSH-ing into a system (via public keys) and providing the cryptographic keys. Note that if the cryptographic key is not externally inputted (i.e. it is in some way embedded on the SD card) then the full-disk-encryption is essentially irrelevant unless you're only trying to defend against very specific attacks (and deleting the key while the machine is running). 

This provides prevention against physical attacks on the device based on just pulling out the SD card and trying to read data (though it would not stop a highly sophisticated attacker from replacing the initial booting image to capture keys - however it stops someone from just grabbing and modifying the SD card with malware or reading your data). 

Here is some information on setting up Full Disk Encryption for different systems - note that most steps here are generic to any system with the appropriate `initrd` generator (like `mkinitramfs` or `mkinitcpio`, as tools used are available on almost all linux systems (you might need to adjust some things like kernels though):
* [Kali Linux on RPi, which uses initramfs and dropbear SSH to allow access for providing the decryption of the main file system](https://www.kali.org/docs/arm/raspberry-pi-with-luks-full-disk-encryption-2/) - also contains a nice script to do this, though you might need to adjust this for different kernel stuff.
* [LUKS on Raspberry Pi - for RasPiOS](https://rr-developer.github.io/LUKS-on-Raspberry-Pi/) - This seems to be another (rather complex) one. It does document the use of much more performant AES cryptography, which is important for the less-powerful systems we can use here. 
* You might be able to make this simpler with a better understanding of [mkinitramfs](https://github.com/zeppe-lin/mkinitramfs) - I currently am using a system based on `mkinitcpio`, which is different.

## Distribution Options
These are the main options for distributions I would suggest to use. Mainly, the most important thing is to try and get the most minimal version of the distro that you can - at the very least, lacking any sort of default Desktop Environment. This is to reduce the number of running services and resource usage, all to help ensure Klipper can function "near-realtime".

We recommend trying to pick a distribution that is "very close" to the stock/upstream form of that distribution (e.g. Debian rather than Ubuntu, Arch rather than Manjaro), or a derivative with minimal modifications (e.g. hardware compatibility plus potentially some things to allow non-interactive setup). 

This is because the "stock" forms of the distribution have the most community attention and usually receive things like security patches quicker, and it's just less likely that some downstream tweaks or defaults will cause issues (especially in combination), especially with "pre-enabled" services. Distributions that perform minimal changes to stock - especially distributions that mostly use the upstream repos directly - are better in a lot of these aspects. 

Something to note is that we mostly recommend Debian-based installations in this guide - this is mostly an artifact of Debian's prominence in the server space, and the fact that most guides are for a Debian system. However, there ARE other options for headless installs, such as Alpine Linux. 

You should, when considering a distribution, always consider the special notes in the [Debian for Raspberry Pi](#debian-for-raspberry-pi) section though, as it applies to certain specific older ARM chips and has implications for what you should pick.

### Alpine Linux Headless
It should be possible to also configure Alpine Linux for headless installation on a decent number of ARM devices. The author has not tried this, but there are good instructions [here][alpine-linux-headless], and it's easy to set up SSH PKA pre-first-boot without serious image wrangling. You can also use the `latest-stable` repositories to create a rolling-release type setup, which many users (like the author) would consider preferrable..

Alpine Linux is an *extremely* lightweight option and seriously worth considering. The site for the actual Linux distribution files is [here][alpine-linux-site]. There is also a specific project that specialises [Alpine Linux for Raspberry Pis][alpine-linux-for-rpi], though you might prefer the ["classic" (as in, the system runs and modifies the disk, as opposed to the way Alpine can run partially or entirely from RAM - on older devices, this is better especially for reducing RAM usage, and it provides better sudden-power-off behaviour) install][alpine-linux-for-rpi-classic].

It also has some instructions on [setting up encrypted root for RPi and presumably other similar devices](https://wiki.alpinelinux.org/wiki/Raspberry_Pi_LVM_on_LUKS), plus some instructions on [generally setting up an encrypted root](https://wiki.alpinelinux.org/wiki/LVM_on_LUKS). Alpine Linux also uses a tool called [mkinitfs](https://gitlab.alpinelinux.org/alpine/mkinitfs), and it should be possible in theory to set up dropbear for unlocking, though the [last guide/code I found is over 7 years old...](https://github.com/mk-f/alpine-initramfs-dropbear).

Something to note though is that Alpine Linux does not use `systemd`. This may be an advantage for some people, and a disadvantage for others. It does, however, mean you cannot use the "standard" `kiauh.sh` (which is a helper for installing and configuring Klipper, and it depends heavily on systemd). The author intends to write their own Klipper administration tool that is portable and easily supports multiple versions of Klipper at some point (using Rust). 

### Debian on ARM
It is possible to make a headless install for [Debian][debian-site] that will get whatever your machine is running. See [information on Debian for Raspberry Pi](#debian-for-raspberry-pi) for specific info about that, as there are important notes on performance there. 

Instructions for a headless install can be found [in the official guide to preseeding][debian-preseeding] and for specifically the "bare minimum" to get SSH with PKA up and running so you can actually do stuff, [in a git repo][debian-headless]. It is, however, worth looking at [Armbian (not exclusively for ARM)](#armbian), primarily for hardware compatibility/performance and the ability to only modify the image a little bit and immediately get a booting system, though it has the disadvantage of being downstream.

It's also worth considering [Alpine Linux](#alpine-linux-headless) for a very lightweight option. It might lack some of the performance tweaks of Armbian, but it is *so* lightweight that there's a good chance it's still worth it. Plus, it just makes the system simpler. 

### Armbian
[Armbian][armbian-site] - which is not exclusively for ARM devices, despite the name - is what I'd recommend you use as a first port-of-call for any devices that *aren't* Raspberry Pis (and honestly it might still be worth it for devices that *are* raspberry pis - unless Debian themselves provides images that can install on it - with an important exception [documented below](#debian-for-raspberry-pi)).

Armbian with the `minimal` or `cli` distribution is very compact. Details on configuring Public-Key-Based SSH for first boot have not yet been written but it should be possible and relatively simple. The basics described in [the security options section](#security-options) should help though. Make sure to pick the "debian-like" rather than "ubuntu-like" versions (as of time of writing, you want Bookworm) - this should keep your system most like upstream debian. 

`Armbian` will run an SSH server automatically on boot. However, they have a very insecure default `root` password, so you really need to (like all the distributions) set up [public-key-authentication-only SSH access](#security-options) BEFORE starting the device (let alone connecting it to any networks).

We recommend this over simply using a Debian headless/netISO installer mostly for hardware compatibility reasons, as well as the fact that they provide images that are very easy to flash and with minimal modification to the image to get things going - but also some performance tweaks designed for embedded uses which are likely to be beneficial to Klipper. However, it's worth considering a Debian Headless install also, and also worth considering [Alpine Linux](#alpine-linux-headless), which is a very very lightweight system. 

### Raspberry Pi OS (Lite) 
[Raspberry Pi OS][rpios-site] is the OS designed by the raspberry pi foundation for Raspberry Pi devices - which is based on Debian. We're most interested in the `Lite` versions, as they are, well, much lighter.  Obviously, this is only really applicable if your device is a Raspberry Pi. 

Raspberry Pi OS is based off of Debian, but targeted specifically to the Raspberry Pi. Most versions of it are somewhat heavyweight, but the `Lite` version is somewhat simpler. Generally speaking, for *most* Raspberry Pi machines I'd suggest using [Armbian](#armbian) or [Debian for Raspberry Pi](#debian-for-raspberry-pi), depending on preferences (Armbian does have some performance tweaks for embedded that should be beneficial for Klipper, though), or [Alpine Linux](#alpine-linux-headless) for those really clean installs.

For newer Raspberry Pis, you should get the 64-bit version of this operating system. 

#### Raspberry Pi OS (Lite) - rpi-imager
The Raspberry Pi Foundation now provides a program called the Raspberry Pi Imager (`rpi-imager`). This will automatically download and flash operating system images for you. Not only this, but you can *use the OS configuration settings menu* to directly set up PKA-only SSH before boot, and make sure you can't access via the password. 

This is a great solution, *if it actually works*. The author had issues where it couldn't write to the device.

For the author (who eventually decided on Alpine Linux instead anyway), they had to manually download the image, flash it, and perform a couple modifications (in particular, setting `.ssh/authorized_keys` in the `pi` user which will eventually get moved, messing with `userconf.txt` in the boot partition, adding an empty file in the boot partition called `ssh`, and manually modifying the `sudoers` config, plus `/etc/ssh/sshd_config` as described in [the security notes](#security-options).


### Debian for Raspberry Pi
There is [a website run by someone from Debian][debrpi-site] which provides Debian pre-built images (along with a simple mechanism to configure Public-Key Access via SSH), for various Raspberry Pi versions, including very old versions.

This should work very well for most Raspberry Pis. HOWEVER - you should NOT use this for any RPi in the RPi Zero group of devices, or in the RPi 1 group of devices. 

This is because the chips in those devices use a particular type of ARM chip which has hardware floating point, but a particular, different kind of hardware floating point than most other ARM chips. Debian does not specifically build for this kind of architecture - they only build images and software for cores of that ARM version without hardware float support (`armel` as opposed to `armhf`). This means that the images on this site use SOFTWARE FLOATING POINT ARITHMETIC for devices using the chipset that Raspberry Pi Zero and RPi 1 devices use.

Klipper in particular does a lot of mathematics, presumably using large amounts of floating point operations. Using software floating point emulation must inevitably cause seriously degraded performance (for Klipper and anything else that does maths, really). So you must avoid any distributions (such as the specific Debian for Raspberry Pi) which don't specifically provide HARDWARE floating point images (`armhf`) for devices using chips with the ARMv6 platform but that have hardware floating point. Make sure images are not tagged with `armel`.

This issue applies most prominently in old Raspberry Pis (Pi 1), or Raspberry Pi Zero devices - make sure your distribution, if it distributes images for those devices, does not use `armel` for them. 

[Raspberry Pi OS](#raspberry-pi-os-lite) is known-good on this front, so at least for the relevant raspberry pi devices, you can use it and be certain. 


[armbian-site]: https://www.armbian.com/download/
[rpios-site]: https://www.raspberrypi.com/software/operating-systems/ 
[rpios-userconf]: https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/
[debian-site]: https://www.debian.org/distrib/
[debian-headless]: https://github.com/philpagel/debian-headless
[debrpi-site]: https://raspi.debian.net/ 
[debian-preseeding]: https://www.debian.org/releases/stable/armhf/apb.en.html
[alpine-linux-headless]: https://github.com/macmpi/alpine-linux-headless-bootstrap
[alpine-linux-site]: https://www.alpinelinux.org/
[alpine-linux-for-rpi]: https://wiki.alpinelinux.org/wiki/Raspberry_Pi
[alpine-linux-for-rpi-classic]: https://wiki.alpinelinux.org/wiki/Classic_install_or_sys_mode_on_Raspberry_Pi
